package utils

import (
	"encoding/json"
	"oauth2-projects/oauth-libs/model"
	"net/http"
	"strings"
)



var (
	GRANT_TYPES  =  [5]string{"authorization_code" , "implicit","password","client_credentials","refresh_token"}
	RESPONSE_TYPES = [2]string{"token","code"}
)


func ValidResponseTypes(inputRespTypes []string) bool {
	sRespTypes := strings.Join(inputRespTypes , ";")
	return strings.Contains(sRespTypes , "token") || strings.Contains(sRespTypes , "code")
}

func ValidResponseTypesString(respTypes , graTypes []string) bool {
	sRespTypes := strings.Join(respTypes , ";")
	sGraTypes := strings.Join(graTypes , ";")

	containCode := strings.Contains(sRespTypes , "code")
	containToken := strings.Contains(sRespTypes , "token")

	vAuthCodeValid := false
	vImplicitValid := false


	if containCode {
		for _ , v := range graTypes {
			if v == "authorization_code"{
				vAuthCodeValid = true
				break
			}
		}
	}

	if containToken {
		for _ , v := range graTypes {
			if v == "implicit" {
				vImplicitValid = true
				break
			}
		}
	}

	if containCode && containToken {
		return vAuthCodeValid && vImplicitValid
	}

	if containCode {
		return vAuthCodeValid && !strings.Contains(sGraTypes , "implicit")
	}

	if containToken {
		return vImplicitValid && !strings.Contains(sGraTypes , "authorization_code")
	}

	return true
}

func ValidGrantTypes(input []string) bool {
	howMuchValid := []bool{}
	for _ , v := range input {
		isValid := false
		for _ , w := range GRANT_TYPES {
			if v == w {
				isValid = true
				break
			}
		}
		howMuchValid = append(howMuchValid , isValid)
	}
	for _ , h := range howMuchValid {
		if !h {
			return false
		}
	}
	return true
}

func IsExistsInArrayString(ss []string, s string ) bool {
	for _ , v := range ss {
		if v == s {
			return true
		}
	}
	return false
}

func WriteApiError(w http.ResponseWriter , statusCode int , errCode , errDesc string) {
	apiErr := model.NewApiError(errCode,errDesc)
	b , _ := json.Marshal(apiErr)
	w.WriteHeader(statusCode)
	w.Write(b)
}