package main

import (
	"oauth2-projects/oauth-libs/handlers"
	"net/http"
	"github.com/gorilla/mux"
)




func main(){


	r := mux.NewRouter()
	r.Path("/register").Methods("POST").
		HandlerFunc(handlers.RegisterHandler)
	r.Path("/oauth/authorize").Methods("POST").
		HandlerFunc(handlers.AuthPost)

	r.Path("/oauth/authorize").Methods("GET").
		HandlerFunc(handlers.AuthGatewayHandler)
	

	http.ListenAndServe(":9090",r)
}