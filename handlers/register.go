package handlers

import (
	"encoding/hex"
	"strings"
	"oauth2-projects/oauth-libs/utils"
	"github.com/rs/xid"
	"oauth2-projects/oauth-libs/model"
	"encoding/json"
	"io/ioutil"
	"net/http"
)


func RegisterHandler(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-type","application/json")
	b , _ := ioutil.ReadAll(r.Body)
	var client model.Client

	err := json.Unmarshal(b, &client)
	if err != nil {
		w.WriteHeader(500)
		b , _ = json.Marshal(model.NewApiError(
			"server_error",
			"Cannot proceed this request",
		))
		return
	}
	client.ClientId = xid.New().String()
	vdGraTypes := utils.ValidGrantTypes(client.GrantTypes)

	if !vdGraTypes {
		apiErr := model.NewApiError("invalid_client_metadata","invalid grant types value")
		b , _ = json.Marshal(apiErr)
		w.WriteHeader(400)
		w.Write(b)
		return
	}
	sGraTypes := strings.Join(client.GrantTypes, ";")
	var ssResp []byte

	if strings.Contains(sGraTypes , "authorization_code") || strings.Contains(sGraTypes , "implicit") {
		vdResTypesString := utils.ValidResponseTypesString(client.ResponseTypes , client.GrantTypes)
		vdResTypes := utils.ValidResponseTypes(client.ResponseTypes)

		if !vdResTypesString  || !vdResTypes {
			apiErr := model.NewApiError("invalid_client_metadata","invalid response types value")
			b , _ = json.Marshal(apiErr)
			w.WriteHeader(400)
			w.Write(b)
			return
		}

	if strings.Contains(sGraTypes , "authorization_code") && !strings.Contains(sGraTypes , "implicit"){
			newSecret := xid.New().String()
			newSecretToHex := hex.EncodeToString([]byte(newSecret))

			if client.TokenEndpointAuthMethod == "none" {
				client.TokenEndpointAuthMethod = "client_secret_basic"
			}

			client.ClientSecret = newSecretToHex
			client.ClientSecretExpiresAt = 0
		}

		ssResp , _ = json.Marshal(client)
		
	} else {
		// password and client_credentials grant types
		// using client secret

		if client.ResponseTypes != nil && len(client.ResponseTypes) > 0 {
			apiErr := model.NewApiError("invalid_client_metadata","invalid response types value")
			b , _ = json.Marshal(apiErr)
			w.WriteHeader(400)
			w.Write(b)
			return
		}

		newSecret := xid.New().String()
		newSecretToHex := hex.EncodeToString([]byte(newSecret))

		if client.TokenEndpointAuthMethod == "none" {
			client.TokenEndpointAuthMethod = "client_secret_basic"
		}

		client.ClientSecret = newSecretToHex
		client.ClientSecretExpiresAt = 0

		ssResp , _ = json.Marshal(client)
	}


	_ , err = model.Db.Exec("insert into oauth2_clients values (?,?,?,?,?,?,?,?,?)",
		client.ClientId,
		client.ClientSecret,
		strings.Join(client.RedirectUris,";"),
		client.TokenEndpointAuthMethod,
		strings.Join(client.GrantTypes,";"),
		strings.Join(client.ResponseTypes,";"),
		client.ClientName,
		client.Scope,
		client.ClientSecretExpiresAt,
	)

	if err != nil {
		apiErr := model.NewApiError("server_error","cannot proceed this request")
		b , _ = json.Marshal(apiErr)
		w.WriteHeader(500)
		w.Write(b)
		return
	}

	w.Write(ssResp)
}