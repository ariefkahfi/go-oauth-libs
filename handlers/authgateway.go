package handlers


import (
	"oauth2-projects/oauth-libs/utils"
	"strings"
	"database/sql"
	"encoding/json"
	"oauth2-projects/oauth-libs/model"
	"net/http"
)



type OauthHandler struct {

}


func (o OauthHandler) AuthPost(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")

	


}

func (o OauthHandler) AuthGatewayHandler(w http.ResponseWriter, r *http.Request) {
	rQuery := r.URL.Query

	// get responseType and other query string values
	// and validate query string values
	rResponseType := rQuery().Get("response_types")
	rClientId := rQuery().Get("client_id")
	rRedirectUri := rQuery().Get("redirect_uri")

	if rResponseType == "" || rClientId == "" || rRedirectUri == "" {
		w.Header().Set("Content-type","application/json")
		apiErr := model.NewApiError("invalid_request","invalid request parameter need [response_types , client_id , redirect_uri]")
		w.WriteHeader(400)
		b , _ := json.Marshal(apiErr)
		w.Write(b)
		return
	}


	// query client in our database
	// to make sure this client have been registered
	row := model.Db.QueryRow("select * from oauth2_clients where client_id = ?",rClientId)
	var clientId , clientSecret, redirectUris, tokenEndpointAuthMethod, grantTypes, responseTypes, clientName, scope string
	var clientSecretExpiresAt int

	err := row.Scan(
		&clientId,
		&clientSecret,
		&redirectUris,
		&tokenEndpointAuthMethod,
		&grantTypes,
		&responseTypes,
		&clientName,
		&scope,
		&clientSecretExpiresAt,
	)

	// send error response , when this client application not registered 
	if err == sql.ErrNoRows {
		utils.WriteApiError(w , 500 , "server_error","This client not registered yet")
		return
	}

	// validate if this rRedirectUri is registered with this client application
	sRedirectUri := strings.Split(redirectUris , ";")
	vdRedirectUri := utils.IsExistsInArrayString(sRedirectUri , rRedirectUri)
	if !vdRedirectUri {
		utils.WriteApiError(w , 400 , "invalid_request","this redirect_uri is not valid")
		return
	}

	// check if this user has logged in authorization_server
	s , _ := model.RediStore.Get(r , "oauth-session")
	username , ok := s.Values["username"]

	if !ok {

		return
	}



	if rResponseType == "code" {
		// grantType must be authorization_code...
	} else if rResponseType == "token" {
		// grantType must be implicit...
	} 
	
}