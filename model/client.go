package model


type Client struct {
	ClientId string `json:"client_id"`
	ClientSecret string `json:"client_secret,omitempty"`
	ClientSecretExpiresAt int `json:"client_secret_expires_at,omitempty"`
	RedirectUris []string `json:"redirect_uris"`
	ClientName string `json:"client_name"`
	TokenEndpointAuthMethod string `json:"token_endpoint_auth_method"`
	GrantTypes []string `json:"grant_types"`
	ResponseTypes []string `json:"response_types"` 
	Scope string `json:"scope"`
}