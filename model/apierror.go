package model

type ApiError struct {
	ErrorCode string `json:"error_code"`
	ErrorDescription string `json:"error_description"`
}

func NewApiError(errorCode, errorDescription string) ApiError {
	return ApiError{
		errorCode,
		errorDescription,
	}
}