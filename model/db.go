package model

import (
	"github.com/go-redis/redis"
	"gopkg.in/boj/redistore.v1"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)


var (
	Db , ErrDb = sql.Open("mysql","arief:arief@/golang_db")
	RediStore , ErrRediDb = redistore.NewRediStore(10 , "tcp",":6739","",[]byte("ASD"))
	RedisClient = redis.NewClient(&redis.Options{
		Addr:":6739",
		DB:0,
	})
)